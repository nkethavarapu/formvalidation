import React, { Component } from "react";
import "./styles.css";

class Main extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      email: "",
      username_error: "",
      password_error: "",
      email_error: "",
    };
  }

  /**
   * Helper function to validate the form upon form submit click
   * @returns true if form validation is succesful, false otherwise
   */
  formValidation = () => {
    // destructuring the state
    let {
      username,
      password,
      email,
      username_error,
      password_error,
      email_error,
    } = this.state;

    let is_username_valid = false;
    let is_password_valid = false;
    let is_email_valid = false;

    // username validations
    if (username === "") {
      username_error = "Please fill this field";
    } else {
      if (!username.match("^(?=[a-zA-Z]{6,9}$)(?!.*[_.]{2})[^_.].*[^_.]$")) {
        username_error = "username is not valid";
      } else {
        username_error = "";
        is_username_valid = true;
      }
    }

    // password validations

    if (password === "") {
      password_error = "Please fill this field";
    } else {
      if (
        !password.match(
          /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/
        )
      ) {
        password_error =
          "Password must contain 1 uppercase,1 lowercase,1 digit,1 special char";
      } else {
        password_error = "";
        is_password_valid = true;
      }
    }

    //email validations
    if (email === "") {
      email_error = "Please fill this field";
    } else {
      if (!email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
        email_error = "Not a valid email";
      } else {
        email_error = "";
        is_email_valid = true;
      }
    }

    this.setState({ username_error, password_error, email_error });
    return is_username_valid && is_password_valid && is_email_valid;
  };
  /**
   * When the input fields are changed
   */

  onChange = (e) => {
    let targetname = [e.target.name];
    this.setState({ [e.target.name]: e.target.value });
    // When input is given ,errors should not display
    if (targetname[0] === "username") {
      this.setState({ username_error: "" });
    } else if (targetname[0] === "password") {
      this.setState({ password_error: "" });
    } else {
      this.setState({ email_error: "" });
    }
  };

  /**
   * When the user clicks on Submit
   * returns either error in respective fields or form submitted successfully
   */

  onSubmit = (e) => {
    e.preventDefault();
    const isValid = this.formValidation();
    if (isValid === true) {
      //send username and password to server
      alert("Form Submitted");
      console.log(this.state);
      this.setState({ username: "", password: "", email: "" });
    }
  };

  clear = () => {
    this.setState({
      username: "",
      password: "",
      email: "",
      username_error: "",
      password_error: "",
      email_error: "",
    });
  };

  render() {
    const { username, password, email } = this.state;
    return (
      <>
        <div class="login-box">
          <h2>𝐋𝐨𝐠𝐢𝐧</h2>
          <form onSubmit={this.onSubmit}>
            <div class="user-box">
              <input
                type="text"
                name="username"
                value={username}
                required=""
                onChange={this.onChange}
              />
              <label>Username</label>
            </div>
            <div className="errormsgsignup">{this.state.username_error}</div>

            <div class="user-box">
              <input
                type="text"
                name="email"
                value={email}
                required=""
                onChange={this.onChange}
              />
              <label>Email</label>
            </div>
            <div className="errormsgsignup">{this.state.email_error}</div>

            <div class="user-box">
              <input
                type="password"
                name="password"
                value={password}
                required=""
                onChange={this.onChange}
              />
              <label>Password</label>
              <div className="errormsgsignup">{this.state.password_error}</div>
            </div>
            <button>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              Submit
            </button>
          </form>
        </div>
      </>
    );
  }
}

export default Main;
